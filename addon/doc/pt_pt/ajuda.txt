1. Utilidade

Serve para guardar e buscar textos espec�ficos e marcadores em p�ginas web,
ou em documentos mostrados no modo de navega��o do NVDA.
O complemento guarda as buscas e marcadores em ficheiros de texto e com
extens�o pickle.
o nome destes arquivos baseia-se no t�tulo do documento actual.
Ao executar o comando de busca espec�fica, o NVDA abrir� um di�logo que
mostrar� o texto correspondente ao documento actual, se se tiver guardado
algun texto para este documento.

2. Comandos


2.1. Teclado

- control+shiftNVDA+s: Abre um di�logo para guardar um texto de busca
associado ao documento actual. Por padr�o, mostra o texto guardado para este
documento. Se se apaga o texto que aparece no di�logo, elimina-se o
correspondente ficheiro de busca.

- control+shift+NVDA+f: Se existir um ficheiro de busca para o documento
actual, abre um di�logo que mostrar� el texto guardado. Ao aceitar, o NVDA
buscar� el texto que se indica na caixa de edi��o. Se n�o se encontrar um
ficheiro para o documento, o NVDA avisar� com uma mensagem, sem abrir o
di�logo de busca espec�fica.


- control+shift+NVDA+k: Guarda a posi��o actual como um marcador. Se se
pressionar duas vezes, apaga o marcador correspondente a esta posi��o.

- control+shift+k: Move-se para o marcador seguinte.

- shift+NVDA+k: Move-se para o marcador anterior.

2.2. Submenu busca espec�fica (NVDA+N

� poss�vel abrir as pastas correspondentes, usando os seguintes items:

- Pasta de busca espec�fica.

- Pasta de marcadores.

- Abrir pasta de informa��o.
